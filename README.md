<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects.

## UNICTIVE API

**How to run project**
- git pull this project
- composer install
- cp .env.example .env
- setup db credentials in .env
- create new db name unictive on your local
- php artisan migrate
- php artisan serve

**Endpoint List**
| Action | Url | Method
| ------ | ------ | ------ |
| Create | /api/register | POST
| Read | /api/profile/?id={id} | GET
| Update | /api/profile/?id={id} | PUT

**Create**

Contoh request untuk create member baru

![image](_apispec/request-create.png)

Akan menghasilkan output seperti berikut ini

![image](_apispec/response-create.png)

Jika mencoba melakukan insert data baru dengan email yang telah terdaftar maka akan di-reject dengan status code 406

![image](_apispec/response-create-email-already-use.png)

**Read**

Untuk melakukan read data user tertentu, dapat dilakukan dengan menyediakan param berupa id dari user tersebut
dengan membawa token dari saat create

![image](_apispec/request-response-read.png)

**Update**

Untuk melakukan update data user tertentu,
dapat dilakukan dengan menyediakan param berupa id dari user tersebut

![image](_apispec/request-update.png)

Akan menghasilkan output seperti ini

![image](_apispec/response-update.png)

Jika diakses kembali ke endpoint read

![image](_apispec/response-after-update.png)
