<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Helpers\SQLErrorHelper;
use App\Models\Member;
use App\Models\Hobby;

class MemberController extends Controller
{

  private function getUser($id) {
    $member = Member::find($id);
    return $member;
  }

  private function getMemberHobby($memberId) {
    $hobby = Hobby::where('member_id', $memberId)->get();
    return $hobby;
  }

  private function getHobby($id) {
    $hobby = Hobby::find($id);
    return $hobby;
  }

  public function profile(Request $request)
  {
    if( empty($request->bearerToken()) ) {
      return response()->json(
        [
          "status" => 'Error',
          "message" => 'Token kosong'
        ],
        400
      );
    }

    if( empty($request->id) ) {
      return response()->json(
        [
          "status" => 'Error',
          "message" => 'Id kosong'
        ],
        400
      );
    }

    $member = $this->getUser($request->id);

    if( is_null($member) ) {
      return response()->json(
        [
          "status" => 'Error',
          "message" => 'Member tidak ditemukan'
        ],
        404
      );
    }

    $hobby = $this->getMemberHobby($request->id);

    return response()->json(["data"=>compact('member','hobby')], 200);

  }

  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'name' => 'required|string',
        'email' => 'required|string|email',
        'phone' => 'required|string',
        'hobby' => 'required|array',
    ]);

    if($validator->fails()){
        return response()->json($validator->errors(), 400);
    }

    if( ! is_numeric($request->get('phone')) ) {
      return response()->json(
        [
          "status" => 'Error',
          "message" => 'Nomor handphone bukan angka'
        ],
        406
      );
    }

    try {
      $member = Member::create([
          'name' => $request->get('name'),
          'email' => $request->get('email'),
          'phone' => $request->get('phone'),
      ]);
    } catch (\Exception $e) {
      return response()->json(
        [
          "status" => 'Error',
          "message" => SQLErrorHelper::message($e->getMessage())
        ],
        406
      );
    }

    $memberId = $member->id;

    try {
      for ($i=0; $i < count($request->get('hobby')); $i++) {
        Hobby::create([
            'member_id' => $memberId,
            'hobby' => $request->get('hobby')[$i]
        ]);
      }
    } catch (\Exception $e) {
      return response()->json(
        [
          "status" => 'Error',
          "message" => $e->getMessage()
        ],
        500
      );
    }

    $token = JWTAuth::fromUser($member);

    return response()->json(compact('member','token'),200);
  }

  public function update(Request $request)
  {
    if( empty($request->bearerToken()) ) {
      return response()->json(
        [
          "status" => 'Error',
          "message" => 'Token kosong'
        ],
        400
      );
    }

    if( empty($request->id) ) {
      return response()->json(
        [
          "status" => 'Error',
          "message" => 'Id kosong'
        ],
        400
      );
    }

    $validator = Validator::make($request->all(), [
        'name' => 'string',
        'email' => 'string|email',
        'phone' => 'string',
        'hobby' => 'array',
    ]);

    $member = $this->getUser($request->id);

    if( is_null($member) ) {
      return response()->json(
        [
          "status" => 'Error',
          "message" => 'Member tidak ditemukan'
        ],
        404
      );
    }

    try {
      if( $request->name ){
        $member->name = $request->name;
      }

      if( $request->email ){
        $member->email = $request->email;
      }

      if( $request->phone ) {
        $member->phone = $request->phone;
      }

      $member->save();
    } catch (\Exception $e) {
      return response()->json(
        [
          "status" => 'Error',
          "message" => SQLErrorHelper::message($e->getMessage())
        ],
        406
      );
    }

    try {
      for ($i=0; $i < count($request->get('hobby')); $i++) {
        $hobby = $this->getHobby($request->get('hobby')[$i]['id']);

        if( $request->get('hobby')[$i]['hobby'] ){
            $hobby->hobby = $request->get('hobby')[$i]['hobby'];
        }
        $hobby->save();
      }
    } catch (\Exception $e) {
      exit('Server Error');
    }

    $hobby = $this->getMemberHobby($request->id);

    return response()->json(["data"=>compact('member', 'hobby')], 200);

  }
}
