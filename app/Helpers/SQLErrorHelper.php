<?php

namespace App\Helpers;

class SQLErrorHelper
{
    public static function message($exceptionMessage): string
    {
        if (str_contains($exceptionMessage, "email_unique")) {
          return 'Email sudah digunakan';
        }
    }
}
