<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hobby extends Model
{
    use HasFactory;

    /**
     *
     * Declare table Name
     *
     */
    protected $table = 'user_hobby';
    

    protected $fillable = [
        'member_id',
        'hobby',
    ];
}
